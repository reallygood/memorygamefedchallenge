## Memory Game FED Challenge

Welcome to your Really Good Front End challenge!

Top chops we’re looking for in this challenge (and in general):

1. Understand requirements, grooming and designing solutions
2. Use web technologies well, from decent HTML & CSS to JS, browsers and the Web.
3. Create solutions with clean code, healthy priorities, third party where it makes sense etc.
4. Work correctly with git and other dev tools
5. Finish on time 😉
6. Any extra thought and care is appreciated

Spec is [here](https://docs.google.com/document/d/1wIJIjDy1wJusDJTe1_X9EXDl0onM2xFnj5SJsEZLVIc/edit?usp=sharing), along with [mockups](https://drive.google.com/open?id=1V9sZmMjzdtoNfANTUKbe-xPnQnYPX1vg) in the Google Drive folder.

Please use Vue.js - Other than that tech stack is up to you (while we do recommend using at least some ES>5).
Fork or clone this repo, please commit often as you make progress, push and submit a pull request when you’re done. Notice BitBucket offers free private repos. If you prefer to work locally and send a zip of the folder, including .git - Your call.

Please edit this readme to contain instructions on how to run this locally (e.g. any npm, yarn, SimpleHTTPServer etc. instructions).

Good luck! 🚀

p.s.
Please use English throughout the challenge. Thanks 🙂
